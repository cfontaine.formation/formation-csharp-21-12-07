﻿using System;

namespace _08_SurchargeOperateur
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Point a = new Point(1, 1);
            Point res = -a;
            Console.WriteLine(res);
            Point b = new Point(3, 2);
            res = a + b;
            Console.WriteLine(res);
            res += new Point(2, -2);    // quand on surcharge un opérateur, l'opérateur composé associé est aussi automatiquement surchargé
            Console.WriteLine(res);
            res *= 2;
            Console.WriteLine(res);
            //res = 2 * res;
            res = res * 2;
            Console.WriteLine(res);
            Console.WriteLine(a == b);
            Console.WriteLine(a != b);
        }
    }
}
