﻿using System;

namespace _09_Delegations
{
    internal class Program
    {

        public delegate int Operation(int a, int b);

        // C# 1.0
        //---------------
        public static int Ajouter(int n1, int n2)
        {
            return n1 + n2;
        }

        public static int Multiplier(int n1, int n2)
        {
            return n1 * n2;
        }
        //---------------

        static void Main(string[] args)
        {
            // C#1.0
            Calculer(2, 3, new Operation(Ajouter));
            Calculer(2, 3, new Operation(Multiplier));
            // C#2.0
            Calculer(2, 3, delegate (int a, int b) { return a + b; });
            Calculer(2, 3, delegate (int a, int b) { return a * b; });
            // C#3.0
            Calculer(2, 3, (v1, v2) => v1 + v2);
            Calculer(2, 3, (v1, v2) => v1 * v2);

            CalculerF(2.4, 4.5, (v1, v2) => v1 + v2);

            CalculerGen(2, 3, (v1, v2) => v1 - v2);
            CalculerGen("2", "3", (v1, v2) => v1 + v2);
            string sep = "azerty";
            CalculerGen("2", "3", (v1, v2) => v1 + sep + v2);
            int scal = 10;
            CalculerGen(2, 3, (v1, v2) => v1 * v2 * scal++);
            Console.WriteLine(scal);

            Action[] action = new Action[2];
            for (int i = 0; i < 2; i++)
            {
                int v = i;
                action[i] = () => Console.WriteLine(v);
            }
            foreach (var a in action)
            {
                a();
            }

            // évenements
            CompteBancaire cb = new CompteBancaire();
            cb.OnSoldeBalance += SmsAlerte;
            cb.OnSoldeBalance += EmailAlerte;
            cb.Debit(20.0M);
            Console.WriteLine(cb.Solde);
            Console.WriteLine("-------------");
            cb.Debit(200.0M);
        }

        static void SmsAlerte(object sender,EventArgs enva)
        {
            CompteBancaire tmp= sender as CompteBancaire;
            Console.WriteLine($"SMS Compte événement {tmp.Solde}");
        }

        static void EmailAlerte(object sender, CompteBancaireEventArgs enva)
        {
            Console.WriteLine($"Email Compte événement {enva.Solde}");

        }

        static void Calculer(int a, int b, Operation op)
        {
            int res = op(a, b);
            Console.WriteLine($"{res}");
        }


        static void CalculerF(double a, double b, Func<double, double, double> op)
        {
            double res = op(a, b);
            Console.WriteLine($"{res}");
        }

        static void CalculerGen<T>(T a, T b, Func<T, T, T> op)
        {
            T res = op(a, b);
            Console.WriteLine($"{res}");
        }
    }
}
