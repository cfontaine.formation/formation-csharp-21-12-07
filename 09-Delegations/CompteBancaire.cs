﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _09_Delegations
{
    internal class CompteBancaire
    {
        public decimal Solde { get; set; } = 50.0M;

        //public delegate void SoldeBalanceDelegate();

        //public event SoldeBalanceDelegate OnSoldeBalance;
        public event EventHandler<CompteBancaireEventArgs> OnSoldeBalance;
        public void Debit(decimal montant)
        {
            Solde-=montant;
            if(Solde < 0 && OnSoldeBalance != null)
            {
                //  OnSoldeBalance(this,EventArgs.Empty);
                OnSoldeBalance(this, new CompteBancaireEventArgs(Solde));
            }
        }

    }
}
