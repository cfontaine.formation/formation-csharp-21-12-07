﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _09_Delegations
{
    internal class CompteBancaireEventArgs : EventArgs
    {
        public decimal Solde { get; set; }

        public CompteBancaireEventArgs(decimal solde)
        {
            Solde = solde;
        }
    }
}
