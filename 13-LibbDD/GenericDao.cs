﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace _13_LibbDD
{
    public abstract class GenericDao<T> where T:DbObject
    {
        public static string ChaineConnection { get; set; }

        protected static SqlConnection cnx;

        public  void SaveOrUpdate(T elm,bool close = true)
        {
            if (elm.Id == 0)
            {
                Create(GetConnection(), elm);
            }
            CloseConnection(close);
        }
        public void Delete(T elm, bool close = true)
        {

        }

        public T FindById(long id,bool close= true)
        {
            T elm = Read(GetConnection(),id);
            CloseConnection(close);
            return elm;
        }

        public List<T> FindAll(bool close = true)
        {
            List<T> list = ReadAll(GetConnection());
            CloseConnection(close);
            return list;
        }

        protected SqlConnection GetConnection()
        {
            if (cnx == null)
            {
                cnx = new SqlConnection(ChaineConnection);
                cnx.Open();
            }
            return cnx;
        }

        protected void CloseConnection(bool close)
        {
            if(cnx!=null && close)
            {
                cnx.Close();
                cnx.Dispose();
                cnx = null;
            }
        }

        protected abstract void Create(SqlConnection cnx, T elm);
        protected abstract T Read(SqlConnection cnx, long id);
        protected abstract List<T> ReadAll(SqlConnection cnx);
    }
}
