﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _13_LibbDD
{
    public abstract class DbObject
    {
        public long Id { get; set; }

        public override bool Equals(object obj)
        {
            return obj is DbObject @object &&
                   Id == @object.Id;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id);
        }

        public override string ToString()
        {
            return string.Format($"{Id}");
        }
    }
}
