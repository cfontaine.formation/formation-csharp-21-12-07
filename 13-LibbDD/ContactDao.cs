﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace _13_LibbDD
{
    public class ContactDao : GenericDao<Contact>
    {
        protected override void Create(SqlConnection cnx, Contact elm)
        {
            string req = "INSERT INTO contacts(prenom,nom,date_naissance,email,telephone) VALUES (@prenom,@nom,@dateNaissnace,@email,@telephone);";
            SqlCommand cmd = new SqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@prenom", elm.Prenom);
            cmd.Parameters.AddWithValue("@nom", elm.Nom);
            cmd.Parameters.AddWithValue("@dateNaissnace", elm.DateNaissance);
            cmd.Parameters.AddWithValue("@email", elm.Email);
            cmd.Parameters.AddWithValue("@telephone", elm.Telphone);
            cmd.ExecuteNonQuery();
            Console.WriteLine((long)cmd?.ExecuteScalar());
        }

        protected override Contact Read(SqlConnection cnx, long id)
        {
            throw new NotImplementedException();
        }

        protected override List<Contact> ReadAll(SqlConnection cnx)
        {
            List<Contact> contacts=new List<Contact>();
            string req = "SELECT id,prenom,nom,date_naissance,email,telephone FROM contacts";
            SqlCommand cmd = new SqlCommand(req, cnx);
            SqlDataReader r = cmd.ExecuteReader();
            while (r.Read())
            {
                Contact tmp = new Contact(r.GetString(1), r.GetString(2), r.GetDateTime(3), r.GetString(4), r.GetString(5));
                tmp.Id = r.GetInt64(0);
                contacts.Add(tmp);
            }
            cnx.Close();
            return contacts;
        }
    }
}
