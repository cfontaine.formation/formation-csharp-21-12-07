﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _15_Linq
{
    internal class Program
    {
        static void Main(string[] args)
        {

            // Method Syntax
            string[] mots = { "Il", "fait", "beau", "Aujourd'hui", "mais", "pas", "demain" };
            IEnumerable<string> motsup3 = mots
            .Where(m => m.Length> 3);
            foreach (var s in motsup3)
            {
                Console.WriteLine(s);
            }

            // Query Syntax
            var motsup4 =
            from mot in mots
            where mot.Length > 4
            select mot;

            foreach (var s in motsup4)
            {
                Console.WriteLine(s);
            }

            var nombres = Enumerable.Range(1, 20);
            var sumQuery= 
                from n in nombres
                where (n%2==0)
                select n;
            Console.WriteLine(sumQuery.Sum());

            List<Contact> contacts = new List<Contact>()
            {
                new Contact("John","Doe",new DateTime(1960,12,5),"jd@dawan.com","0320000001"),
                new Contact("Jane","Doe",new DateTime(1989,1,15),"janed@dawan.com","0320000002"),
                new Contact("Jo","Dalton",new DateTime(1987,4,1),"joed@dawan.com","0320000003"),
                new Contact("Allan","Smithee",new DateTime(2000,7,4),"asm@dawan.com","0320000004")
            };

            var contactQuery =
                from co in contacts
                where ((co.DateNaissance > (new DateTime(1988, 1, 1))))
                select co;

            foreach (Contact c in contactQuery)
            {
                Console.WriteLine(c);
            }

            var contactQuery2 = contacts
             .Where(c => ((c.Nom).StartsWith("J")))
             .OrderBy(c => c.Nom.Length);

            foreach (Contact c in contactQuery2)
            {
                Console.WriteLine(c);
            }

        }
    }
}
