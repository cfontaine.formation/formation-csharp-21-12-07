﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _15_Linq
{
   public class Contact 
    {
        public Contact(string nom, string prenom, DateTime dateNaissance, string email, string telphone)
        {
            Nom = nom;
            Prenom = prenom;
            DateNaissance = dateNaissance;
            Email = email;
            Telphone = telphone;
        }

        public string Nom { get; set; }
        public string Prenom { get; set; }
        public DateTime DateNaissance { get; set; }
        public string Email { get; set; }
        public string Telphone { get; set; }

        public override string ToString()
        {
            return string.Format($"{base.ToString()}{Prenom} {Nom} {DateNaissance} {Email}");
        }
    }
}
