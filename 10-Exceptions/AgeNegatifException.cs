﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _10_Exceptions
{
    internal class AgeNegatifException : Exception
    {
        public int Age { get; }
        public AgeNegatifException(int age)
        {
            Age = age;
        }

        public AgeNegatifException(int age,string message) : base(message)
        {
            Age = age;
        }

        public AgeNegatifException(int age, string message, Exception innerException) : base(message, innerException)
        {
            Age = age;
        }
    }
}
