﻿using System;

namespace _10_Exceptions
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int[] t = new int[5];
                int index = Convert.ToInt32(Console.ReadLine());    // Peut générer une Exception FormatException, si on saisie autre chose qu'un nombre entier
                Console.WriteLine(t[index]);                        // Peut générer une Exception IndexOutOfRangeException , si index est > 
                //File.OpenRead("Nexistepas");                      // Génère une Exception FileNotFoundException
                int age = Convert.ToInt32(Console.ReadLine());
                TraitementEmploye(age);
                Console.WriteLine("Suite du programme");
            }

            catch (IndexOutOfRangeException e)   // Attrape les exceptions IndexOutOfRangeException
            {
                Console.WriteLine("Erreur: index en dehors du tableau");
                Console.WriteLine(e.Message);   // Message => permet de récupérer le messsage de l'exception
                Console.WriteLine(e.StackTrace);
            }
            catch (FormatException e)     // Attrape les exceptions FormatException
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e) // Attrape tous les autres exception 
            {
                Console.WriteLine("Traitement Exception");
                Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message);
                    Console.WriteLine(e.InnerException.StackTrace);
                }
            }
            finally     // Le bloc finally est toujours éxécuté
            {           // finally est utilisé pour libérer les ressources
                Console.WriteLine("Toujours Executé");
            }
            Console.WriteLine("Fin du programme");
        }
        static void TraitementEmploye(int age)
        {
            try
            {
                Console.WriteLine("Début Traitement Employe");
                TraitementAge(age);
                Console.WriteLine("Fin Traitement Employe");
            }
            catch (AgeNegatifException e) when (e.Age < -10)   // when : le catch est éxécuté si la condition dans when est vrai
            {
                // Traitement partiel de l'exception AgeNegatifException
                Console.WriteLine($"traitement Employe: {e.Message}");
                // On relance l'exception
                throw new ArgumentException("age negatif traitement Employé", e);     // e =>référence à l'exception qui a provoquer l'exception
            }
            catch (AgeNegatifException e)
            {
                // Traitement partiel de l'exception AgeNegatifException
                Console.WriteLine($"traitement Employe: {e.Message}");
                Console.WriteLine("-------> age >-10");
                throw;      // On relance l'exception pour que l'utilisateur de la méthode la traite a son niveau
            }

        }
        static void TraitementAge(int age)
        {
            Console.WriteLine("Début Traitement age");
            if (age < 0)
            {
                throw new AgeNegatifException(age, "age negatif");   //  throw => Lancer un exception
                                                                     //  Elle va remonter la pile d'appel des méthodes jusqu'à ce qu'elle soit traité par un try/catch
            }                                                       //  si elle n'est pas traiter, aprés la méthode main => arrét du programme
            Console.WriteLine("Fin Traitement age");
        }
    }
}
