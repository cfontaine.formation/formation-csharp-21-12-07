﻿using System;

namespace _03_Instructions
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Condition_switch
            // switch clause when c# 7.0
            int jours = 7;
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    break;
                case int j when j == 6 || j == 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // switch sur les types c# 7.0
            object x = "123";

            switch (x)
            {
                case int i:
                    Console.WriteLine("x est un  entier ={i}");
                    break;
                case string sw:
                    Console.WriteLine("x est une chaine de caractère " + sw.Length);
                    break;
                default:
                    Console.WriteLine("x ???");
                    break;
            }

            // switch avec les corps d'expression (C# 8.0)
            string jourStr = jours switch
            {
                1 => "Lundi",
                6 => "Week-end",
                7 => "Week-end",
                _ => "Autre jour"
            };
            Console.WriteLine(jourStr);
            #endregion

            #region Instructions de branchement
            for (int i = 0; i < 10; i++)
            {

                if (i == 3)
                {
                    break;  // break => termine la boucle
                }
                Console.WriteLine(i);
            }

            for (int i = 0; i < 10; i++)
            {
                if (i == 3)
                {
                    continue;    // continue => on passe à l'itération suivante
                }
                Console.WriteLine(i);
            }


            //goto
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Console.WriteLine($"i={i} j={j}");
                    if (i == 3)
                    {
                        goto EXIT_LOOP; // Utilisation de goto pour se branche sur le label EXIT_LOOP et quitter les 2 boucles imbriquées
                    }
                }
            }

        EXIT_LOOP: int jour = 1;
            switch (jour)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    //goto case 6;
                    goto default;    // Utilisation de goto pour transférer le contrôle à un case ou à l’étiquette par défaut d’une instruction switch
                case 6:
                case 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            jours = 1;
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    // goto case 7;
                    goto default;
                case 6:
                case 7:

                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }
            #endregion

            #region Format de chaine de caractères
            int xi = 1;
            int yi = 5;
            string resFormat = string.Format("xi={0} yi={1}", xi, yi);
            Console.WriteLine(resFormat);

            Console.WriteLine("xi={0} yi={1}", xi, yi);     // on peut définir directement le format dans la mèthode WriteLine 

            Console.WriteLine($"xi={xi} yi={yi}");

            Console.WriteLine("c:\tmp\newfile.txt");
            // Chaînes textuelles => @ devant une littérale chaine de caractères
            // Conserve les retours à la ligne, n'interpréte pas les caractères spéciaux (utile pour les chemins de fichiers) 
            Console.WriteLine(@"c:\tmp\newfile.txt");
            #endregion


            // Exercice: Calculatrice
            // Faire un programme calculatrice
            // Saisir dans la console
            // un double v1
            // une chaine de caractère opérateur qui a pour valeur valide: + - * /
            // un double v2

            // Afficher:
            // Le résultat de l’opération
            // Une message d’erreur si l’opérateur est incorrecte
            // Une message d’erreur si l’on fait une division par 0
            double v1 = Convert.ToDouble(Console.ReadLine());
            string op = Console.ReadLine();
            double v2 = Convert.ToDouble(Console.ReadLine());

            string resultat = op switch
            {
                "+" => $"{v1} + {v2} = {v1 + v2}",
                "-" => $"{v1} - {v2} = {v1 - v2}",
                "*" => $"{v1} * {v2} = {v1 * v2}",
                "/" => v2 == 0.0 ? "Division par 0" : $"{v1} / {v2} = {v1 / v2}",
                _ => $"L'opérateur {op} est invalide"
            };
            Console.WriteLine(resultat);

            // Série harmonique
            // On saisit un nombre entier __n__ et on calcule la somme des n premiers termes de la série harmonique:
            // 1 + 1 / 2 + 1 / 3 + 1 / 4 + ... +1 / n
            int n = Convert.ToInt32(Console.ReadLine());
            double harmonique = 1.0;
            if (n > 0)
            {
                for (int i = 2; i <= n; i++)
                {
                    harmonique += 1.0 / i;
                }
                Console.WriteLine(harmonique);
            }

        }

    }
}
