﻿using System;
using System.Collections.Generic;

namespace _11_Collections
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<string> lstStr = new List<string>();
            lstStr.Add("hello");
            lstStr.Add("World");
            lstStr.Add("Bonjour");
            //lstStr.Add(2.0);
            string resStr = lstStr[1];
            Console.WriteLine(resStr);
            lstStr[0] = "-------------------";
            Console.WriteLine(lstStr.Count);

            Console.WriteLine(lstStr.Count);

            
            foreach(var s in lstStr)
            {
                Console.WriteLine(s);
            }
            lstStr.Reverse();
            IEnumerator<string> it=lstStr.GetEnumerator();
            while (it.MoveNext())
            {
                Console.WriteLine(it.Current);
            }

            foreach(var i in GetPairValeur(64))
            {
                Console.WriteLine(i);
            }

            IEnumerator<int> it2 = GetPairValeur().GetEnumerator();
            while (it2.MoveNext())
            {
                Console.WriteLine(it2.Current);
            }

            Dictionary<int,string> m= new Dictionary<int,string>();
            m.Add(12, "Bonjour");
            m.Add(912, "World");
            m.Add(34, "Hello");
            m.Add(56, "Asupprimer");
            Console.WriteLine(m[34]);
            m[34] = "--------";
            m.Remove(56);

            foreach(var kv in m)
            {
                Console.WriteLine($"clé={kv.Key} valeur={kv.Value}");
            }
        }

        static IEnumerable<int> GetPairValeur(int limit = 20)
        {
            int i = 0;
            do
            {
                if (i % 2 == 0)
                {
                    yield return i;
                }

            } while (i++ < limit);
        }
    }
}
