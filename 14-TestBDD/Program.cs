﻿using _13_LibbDD;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace _14_TestBDD
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Contact c = new Contact("Doe", "John", DateTime.Now, "jd@dawan.com", "0320000001");
            //TestBdd(c);
            TestDao(c);
        }

        static void TestBdd(Contact c)
        {
            string chaineConnection = @"Data Source=DESKTOP-9H6VFME\SQLEXPRESS;Initial Catalog=formation;Integrated Security=True";
            SqlConnection cnx = new SqlConnection(chaineConnection);
            cnx.Open();

            string req = "INSERT INTO contacts(prenom,nom,date_naissance,email,telephone) VALUES (@prenom,@nom,@dateNaissnace,@email,@telephone);";
            SqlCommand cmd = new SqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@prenom", c.Prenom);
            cmd.Parameters.AddWithValue("@nom", c.Nom);
            cmd.Parameters.AddWithValue("@dateNaissnace", c.DateNaissance);
            cmd.Parameters.AddWithValue("@email", c.Email);
            cmd.Parameters.AddWithValue("@telephone", c.Telphone);
            cmd.ExecuteNonQuery();
            req = "SELECT prenom,nom,date_naissance FROM contacts";
            cmd = new SqlCommand(req, cnx);
            SqlDataReader r=cmd.ExecuteReader();
            while (r.Read())
            {
                Console.WriteLine($"{ r.GetString(0)} {r.GetString(1)} {r.GetDateTime(2)}");
            }
            cnx.Close();
        }

        static void TestDao(Contact c)
        {
            ContactDao.ChaineConnection = @"Data Source=DESKTOP-9H6VFME\SQLEXPRESS;Initial Catalog=formation;Integrated Security=True";
            ContactDao dao = new _13_LibbDD.ContactDao();
            dao.SaveOrUpdate(c,false);
            List<Contact> lst = dao.FindAll();
            foreach(Contact co in lst)
            {
                Console.WriteLine(co);
            }
        }
    }
}
