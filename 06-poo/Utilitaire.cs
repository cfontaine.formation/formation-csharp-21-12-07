﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _06_poo
{
   // sealed
    internal class Utilitaire : Voiture
    {
        public double Volume { get; set; }

        public Utilitaire(double volume) // base()
        {
            Volume = volume;
        }

        public sealed override void Afficher()
        {
            base.Afficher();
            Console.WriteLine($"{Volume}");
        }
    }
}
