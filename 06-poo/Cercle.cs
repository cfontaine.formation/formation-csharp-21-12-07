﻿namespace _06_poo
{
    internal class Cercle
    {
        public Point Centre { get; set; }

        public int Rayon { get; set; } = 1;

        public Cercle()
        {
        }

        public Cercle(Point centre, int rayon)
        {
            Centre = centre;
            Rayon = rayon;
        }

        public static bool Collision(Cercle c1, Cercle c2) => Point.Distance(c1.Centre, c2.Centre) <= (c1.Rayon + c2.Rayon);

        public bool Contenu(Point p) => Point.Distance(Centre, p) <= Rayon;
    }
}
