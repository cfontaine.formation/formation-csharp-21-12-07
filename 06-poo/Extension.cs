﻿namespace _06_poo
{
    internal static class Extension
    {
        public static string Inverser(this string str)
        {
            string strInv = "";
            for (int c = 1; c <= str.Length; c++)
            {
                strInv += str[^c];
            }
            return strInv;
        }
    }
}
