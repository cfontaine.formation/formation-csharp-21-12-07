﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _06_poo
{
    internal class Phrase
    {
        string[] _mots;

        public Phrase(string phrase)
        {
            _mots = phrase.Split(' ');
        }

        public string this[int index]
        {
            get
            {
                if (index < _mots.Length)
                {
                    return _mots[index];
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
            set
            {
                if (index < _mots.Length)
                {
                    _mots[index]=value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }

        public string this[Index index] => _mots[index];

        public string[] this[Range range] => _mots[range];
        public void Afficher()
        {
            foreach(string s in _mots)
            {
                Console.Write($"{s} ");
            }
            Console.WriteLine();
        }
    }
}
