﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _06_poo
{
    internal class Canard : IPeutMarcher, IPeutVoler
    {
        public void Atterir()
        {
            Console.WriteLine("Le canard attérie");
        }

        public void Courrir()
        {
            Console.WriteLine("Le canard marche");
        }

        public void Decoler()
        {
            Console.WriteLine("Le canard décoler");
        }

        public void Marcher()
        {
            Console.WriteLine("Le canard court");
        }
    }
}
