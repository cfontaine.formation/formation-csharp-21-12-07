﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _06_poo
{
    internal static class Extension2
    {
        public static string Inverser2(this string str)
        {
            string strInv = "";
            for (int c = 1; c <= str.Length; c++)
            {
                strInv += str[^c];
            }
            return strInv;
        }
    }
}

