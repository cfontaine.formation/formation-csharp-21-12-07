﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _06_poo
{
    internal class Chien : IPeutMarcher
    {
        public string Nom { get; set; }

        public void Courrir()
        {
            Console.WriteLine("Le Chien marche");
        }

        public void Marcher()
        {
            Console.WriteLine("Le Chien court");
        }
    }
}
