﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _06_poo
{
    internal class Conteneur
    {
        private static string vClasse = "Variable de classe";

        private string vInstance = "Variable d'instance";

        private protected string vInst2= "Variable d'instance";

        public void Test()
        {
            Element element = new Element();
            element.TestVarClasse();
            element.TestVarInstance(this);
        }

        private class Element
        {
            public void TestVarClasse()
            {
                Console.WriteLine(vClasse);
            }

            public void TestVarInstance(Conteneur c)
            {
                Console.WriteLine(c.vInstance);
                Console.WriteLine(c.vInst2);
            }
        }

        
    }
}
