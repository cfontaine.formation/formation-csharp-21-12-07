﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _06_poo
{
    internal interface IPeutMarcher
    {
        void Marcher();
        void Courrir();

        void Trotinner() => Console.WriteLine("Trotinner");

        static string Message;
    }
}
