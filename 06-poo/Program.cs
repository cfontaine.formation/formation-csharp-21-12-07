﻿using System;

namespace _06_poo
{
    internal class Program
    {
        static void Main(string[] args)
        {
        /*    //  Console.WriteLine(Voiture.cptVoiture);
            Voiture v1 = new Voiture();
            // v1.Marque = "opel";
            Console.WriteLine(v1.Marque);
            v1.Accelerer(10);
            v1.Afficher();
            // v1.Vitesse = 3;
            v1 = null;
            GC.Collect();

            Voiture v2 = null;
            // v2.Accelerer(12); // Exception
            v2?.Accelerer(12);
            Console.WriteLine(v2 != null && v2.EstArreter());
            bool? tst = v2?.EstArreter();
            if (tst.HasValue)
            {
                Console.WriteLine(tst.Value);
            }
            v2 = new Voiture("Ford", "Jaune", "az-1346-fr");
            v2.Afficher();
            v2.Accelerer(10);
            v2.Afficher();
            v2.Arreter();
            v2.Afficher();

            // Voiture v3 = new Voiture() { marque="Fiat"};
            Voiture v3 = new Voiture() { Couleur = "Jaune" };
            v3.Afficher();
            (string marque, int vitesse) = v3;
            Console.WriteLine($"{marque} {vitesse}");
            var (m, v) = v3;
            Console.WriteLine($"{m} {v}");

            Console.WriteLine(Voiture.EgaliteVitesse(v2, v3));

            // Indexeur
            Phrase p = new Phrase("Le temp est bon aujourd'hui");
            p.Afficher();
            p[3] = "mauvais";
            p.Afficher();

            Console.WriteLine(p[^1]);
            string[] m2 = p[1..4];
            foreach (string s in m2)
            {
                Console.WriteLine(s);
            }
            Form1 f1 = new Form1(12);
            f1.Data = 3;
            f1.Afficher();

            Conteneur co = new Conteneur();
            co.Test();

            //Math m = new Math();
            Math.Abs(10);

            string testExt = "Bonjour";
            string resInv = testExt.Inverser();
            Console.WriteLine(resInv);

            Point p1 = new Point();
            p1.Afficher();
            p1.Deplacer(1, 1);
            p1.Afficher();
            Point p10 = new Point(10, 10);
            p1.Afficher();
            Cercle c1 = new Cercle(new Point(2, 0), 2);
            Cercle cc = new Cercle(new Point(6, 0), 3);
            Cercle cnc = new Cercle(new Point(10, 0), 1);
            Console.WriteLine(Cercle.Collision(c1, cc));
            Console.WriteLine(Cercle.Collision(c1, cnc));
            Console.WriteLine(c1.Contenu(p1));
            Console.WriteLine(c1.Contenu(p10));
        */
            // Héritage
            VoiturePrioritaire vp = new VoiturePrioritaire("Subaru", "Bleu", "za-1245-hk");
            vp.Afficher();

            // Polymorphisme
            Voiture v4 = new VoiturePrioritaire("Subaru", "Bleu", "za-4356-hk");
            v4.Afficher();
            v4.Detail();

             // VoiturePrioritaire vp2 = (VoiturePrioritaire) v3;
            //if(v3 is VoiturePrioritaire)
            //{ 
            //    VoiturePrioritaire vp2 = v3 as VoiturePrioritaire;
            //    Console.WriteLine(vp2.Gyro);
            //}

            if (v4 is VoiturePrioritaire vp3)
            {
                Console.WriteLine(vp3.Gyro);
            }
            Console.WriteLine(v4);

            Voiture v5 = new VoiturePrioritaire("Subaru", "Bleu", "za-4356-hk");
            Console.WriteLine(v5 == v4); //false
            Console.WriteLine(v5.Equals(v4));

            Type ty = v4.GetType();

            Voiture v6 = new Utilitaire(20);
            v6.Afficher();

            IPeutMarcher ip1 = new Chien();
            ip1.Courrir();
            ip1.Trotinner();
            IPeutMarcher.Message = "azerty";
        }


    }
}
