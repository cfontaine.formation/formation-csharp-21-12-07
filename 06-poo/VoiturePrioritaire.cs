﻿using System;

namespace _06_poo
{
    internal class VoiturePrioritaire : Voiture
    {

        public bool Gyro { get; private set; }
        public VoiturePrioritaire(string marque, string couleur, string plaqueIma,bool gyro=false) : base(marque, couleur, plaqueIma)
        {
            Gyro = gyro;
        }

        public void AlumerGyro()
        {
            Gyro = true;
        }

        public void EteindreGyro()
        {
            Gyro = false;
        }

        public override void Afficher()
        {
            base.Afficher();
            Console.WriteLine($"Gyro {Gyro} ");
        }

        public new void Detail()
        {
            Console.WriteLine("Voiture prioritaire");
            base.Detail();
        }

        public override string ToString()
        {
            return string.Format($"{base.ToString()} Gyro {Gyro}");
        }

        public override bool Equals(object obj)
        {
            return obj is VoiturePrioritaire prioritaire &&
                   base.Equals(obj) &&
                   Gyro == prioritaire.Gyro;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), Gyro);
        }
    }
}
