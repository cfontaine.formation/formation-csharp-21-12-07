﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _06_poo
{
    internal partial class Form1
    {
        public Form1(int data)
        {
            Data = data;
        }
        public void Afficher()
        {
            Console.WriteLine(Data);
        }
    }
}
