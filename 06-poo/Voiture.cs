﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _06_poo
{
    abstract class Voiture
    {
        //private string _marque;
        string _couleur ="Noir";
       // string plaqueIma = default; // null
        //int vitesse;
       // int compteurKm = 200;

        public string Marque { get; }
        public string PlaqueIma { get; set; }

        public int Vitesse { get; private set; }

        public int CompteurKm { get; } = 200;

        public static int CptVoiture { get; private set; }
        public Voiture() // :this("Opel","rouge","ea-345-rt")
        {
            CptVoiture++;
        }
        public Voiture(string marque,string couleur, string plaqueIma) : this()
        {
            Marque = marque;
            _couleur = couleur;
            PlaqueIma = plaqueIma;
        }

        public Voiture(string marque, string couleur, string plaqueIma, int vitesse, int compteurKm) : this(marque, couleur, plaqueIma)
        {
            Vitesse = vitesse;
            CompteurKm = compteurKm;
        }

        ~Voiture()
        {
            Console.WriteLine("Destructeur");
        }

        public void Deconstruct(out string m, out int v)
        {
            m = Marque;
            v = Vitesse;
        }

        //public string Couleur
        //{
        //    get
        //    {
        //        return _couleur;
        //    }
        //    set
        //    {
        //        _couleur = value;   
        //    }
        //}

        // C# 7.0
        public string Couleur
        {
            get => _couleur;
            set => _couleur = value;
        }

        

        public void Accelerer(int vAcc)
        {
            if(vAcc >= 0)
            {
                Vitesse += vAcc;
            }
        }

        public void Freiner(int vFrn)
        {
            if (vFrn >= 0)
            {
                Vitesse -= vFrn;
            }
        }

        public void Arreter()
        {
            Vitesse = 0;
        }

        public bool EstArreter()
        {
            return Vitesse == 0;
        }

        public virtual void Afficher()
        {
            Console.WriteLine($"{Marque} {_couleur} {PlaqueIma} {Vitesse} {CompteurKm}");
        }
        public void Detail()
        {
            Console.WriteLine($"{Marque} {_couleur} ");
        }

        public static void TestMethodeClasse()
        {
            Console.WriteLine("Méthode de classe");
            Console.WriteLine($"Variable de classe {CptVoiture}");
            // vitesse = 3;
            // EstArreter();
        }

        public static bool EgaliteVitesse(Voiture v1, Voiture v2)
        {
            return v1.Vitesse == v2.Vitesse;
        }

        public override string ToString()
        {
            return string.Format($"{Marque} {_couleur} {PlaqueIma} {Vitesse} {CompteurKm}");
        }

        public override bool Equals(object obj)
        {
            return obj is Voiture voiture &&
                   _couleur == voiture._couleur &&
                   Marque == voiture.Marque &&
                   PlaqueIma == voiture.PlaqueIma &&
                   Vitesse == voiture.Vitesse &&
                   CompteurKm == voiture.CompteurKm &&
                   Couleur == voiture.Couleur;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_couleur, Marque, PlaqueIma, Vitesse, CompteurKm, Couleur);
        }
    }
}
