﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _04_Methodes
{
    internal class Class1
    {
        // Dans le cas où le projet contient plusieurs Classe contenant un Main
        // Il faut choisir l'objet qui va démmarer 
        // Dans visual studio: propriétés du projet -> Application -> Général -> Objet de démarrage
        static void Main()
        {
            Console.WriteLine("Other main");
        }
    }
}
