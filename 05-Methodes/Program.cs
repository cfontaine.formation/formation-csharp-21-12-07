﻿using System;
using System.Text;

namespace _04_Methodes
{

    public struct BigData
    {
        public byte[] data;

        public BigData(int s)
        {
            data = new byte[s];
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            // Passage par valeur (par défaut en C#)
            // C'est une copie de la valeur du paramètre qui est transmise à la méthode
            // Avec un objet c'est la référence qui est copié, si l'état de l'objet est modifié 
            StringBuilder sb = new StringBuilder("aaaa");
            TestParamValeur(sb);
            Console.WriteLine(sb);

            // Passage de paramètre par référence
            // La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
            int a = 1;
            TestParamRef(ref a);
            Console.WriteLine(a);

            // Passage de paramètre en sortie
            // Un paramètre out ne sert à la méthode qu'à retourner une valeur
            // L'argument transmis doit référencer une variable ou un élément de tableau
            // La variable n'a pas besoin d'être initialisée

            // bool b;
            TestParamOut(out a, out bool b);    //On peut déclarer la variable de retour dans les arguments pendant l'appel de la méthode
            Console.WriteLine($"{a} {b}");

            // On peut ignorer un paramètre out en le nommant _
            TestParamOut(out _, out b);
            Console.WriteLine($"{b}");

            // Passage par référence avec in
            // identique à ref, sauf que l'argument ne peut pas être modifiée par la méthode -> une erreur de compilation
            // Utile avec le passage de type valeur pour éviter une copie
            BigData big = new BigData(10);
            TestParamIn(big);
            Console.WriteLine("data=" + big.data[3]);

            // Paramètre optionnel
            TestParamOptionnel(1);
            TestParamOptionnel(2, "azerty");
            TestParamOptionnel(2, "------", 'b');

            // Paramètres nommés
            TestParamOptionnel(c1: 'e', a: 2);
            TestParamOptionnel(4, c1: 'f');

            // Nombre de paramètres variable
            Console.WriteLine(Moyenne(4));
            Console.WriteLine(Moyenne(3, 5, 2, 5));
            int[] tVal = { 10, 3, 67 }; // On put aussi passer un tableau
            Console.WriteLine(Moyenne(3, tVal));

            // Affichage des paramètres passés au programme (main)
            // En lignes de commandes: 05-methode.exe Azerty 12234 "aze rty"
            // Dans visual studio: propriétés du projet-> onglet Déboguer-> profil de lancement de débogage -> Arguments de la ligne de commande 
            foreach (var par in args)
            {
                Console.WriteLine(par);
            }

            // Méthode locale
            // Mult(2, 4);  // la méthode local n'est pas 
            Console.WriteLine(TestMethodeLocal(3));
            Console.WriteLine(TestMethodeLocalStatic(3));

            // Tupple
            // Déclaration et Initialisation
            (double d, int i) testTupples = (3.14, 42);

            // Récupération des valeurs
            Console.WriteLine($"{testTupples.Item1} {testTupples.Item2}");
            Console.WriteLine($"{testTupples.d} {testTupples.i}");

            // Utilisation avec le retour d’une fonction
            var p = CalculCoordonne();
            Console.WriteLine($"{p.Item1} {p.Item2}");
            var (p1, p2) = CalculCoordonne();
            Console.WriteLine($"{p1} {p2}");

            double d1, d2;
            (d1, d2) = CalculCoordonne();
            Console.WriteLine($"{d1} {d2}");
        }

        // Passage par valeur
        static void TestParamValeur(StringBuilder sb)
        {
            // sb = new StringBuilder("-------------"); // modifie la référence
            sb.Append("bbbbbbbb");
        }

        // Passage par référence => ref
        static void TestParamRef(ref int par)
        {
            par = 42;   // La modification de la valeur du paramètre par n'a pas d'influence en dehors de la méthode
        }

        // Passage de paramètre en sortie => out
        static void TestParamOut(out int par1, out bool par2)
        {
            par1 = 84;  // La méthode doit obligatoirement affecter une valeur aux paramètres out
            Console.WriteLine(par1);
            par2 = false;
        }

        //in => comme un passage par valeur, mais on évite les copies (Passage par référence en lecture seule)
        static void TestParamIn(in BigData p)
        {
            // p = new BigData(20); // erreur uniquement en lecture
            foreach (var c in p.data)
            {
                Console.WriteLine(c);
            }
            p.data[3] = 3;
        }

        // On peut donner une valeur par défaut aux arguments passés par valeur
        static void TestParamOptionnel(int a, string str = "default", char c1 = 'a')
        {
            Console.WriteLine($"{a} {str} {c1}");
        }

        // Nombre d'arguments variable => params
        static double Moyenne(int v1, params int[] vs)
        {
            double somme = v1;
            foreach (var v in vs)
            {
                somme += v;
            }
            return somme / (1 + vs.Length);
        }

        // Corps d'expression =>
        static double Somme(double a, double b) => a + b;

        // Méthode Locale
        static double TestMethodeLocal(int par)
        {
            double a = 2.3;
            return Mult(2.4, 5.6);

            double Mult(double v1, double v2) => v1 * v2 * par * a;
        }

        // Méthode locale statique (aux variables locales et aux paramètres de la méthode englobante
        static double TestMethodeLocalStatic(int par)
        {
            //double a = 2.3;
            return Mult(2.4, 5.6);

            static double Mult(double v1, double v2) => v1 * v2;//* par * a;
        }

        // Tupple (Utilisation avec le retour d’une fonction)
        static (double co1, double co2) CalculCoordonne()
        {
            //..
            return (1.0003, -12.6);
        }
    }
}
