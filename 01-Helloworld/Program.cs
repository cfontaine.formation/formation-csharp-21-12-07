﻿using System;

namespace _01_Helloworld
{
    /// <summary>
    /// Classe Hello World
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Point d'entrée du programme
        /// </summary>
        /// <param name="args"> argument de la ligne de commande</param>
        static void Main(string[] args)     // Un commentaire fin de ligne
        {
            /*
            * Commentaire 
            * sur plusieurs lignes
            */
            Console.WriteLine("Hello World!");
        }
    }
}
