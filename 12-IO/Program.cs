﻿using System;
using System.IO;

namespace _12_IO
{
    internal class Program
    {
        static void Main(string[] args)
        {
            InfoDrive();
            InfoRep(@"C:\Dawan\Formations\TestIO\C#");
            TestFichier(@"C:\Dawan\Formations\TestIO\C#\asuprimer.txt");
            Parcourir(@"C:\Dawan\Formations\TestIO\C#");
            TestEcrireFicText(@"C:\Dawan\Formations\TestIO\C#\test.txt");
            TestLireFicText(@"C:\Dawan\Formations\TestIO\C#\test.txt");
        }

        static void InfoDrive()
        {
            DriveInfo[] le = DriveInfo.GetDrives();
            foreach (var d in le)
            {
                Console.WriteLine(d.Name);
                Console.WriteLine(d.TotalSize);
                Console.WriteLine(d.TotalFreeSpace);
                Console.WriteLine(d.DriveFormat);
            }
        }

        static void InfoRep(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        static void Parcourir(string path)
        {
            if (Directory.Exists(path))
            {
                string[] fi = Directory.GetFiles(path);
                foreach (string f in fi)
                {
                    Console.WriteLine($"\t={f}");
                }
                string[] rep = Directory.GetDirectories(path);
                foreach (string r in rep)
                {
                    Console.WriteLine($"Répertoire={r}");
                    Console.WriteLine($"______________");
                    Parcourir(r);
                }
            }
            else
            {
                Directory.CreateDirectory(path);
            }
        }

        static void TestFichier(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        static void TestEcrireFicText(string path)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(path, true);
                for (int i = 0; i < 10; i++)
                {
                    sw.WriteLine("Hello world");
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                sw?.Close();
                sw?.Dispose();
            }

        }

        static void TestLireFicText(string path)
        {
            if (File.Exists(path))
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    while (!sr.EndOfStream)
                    {
                        string row = sr.ReadLine();
                        Console.WriteLine(row);
                   }
                }
            }

        }


    }
}
