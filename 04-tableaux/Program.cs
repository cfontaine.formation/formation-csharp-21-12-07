﻿using System;

namespace _04_tableaux
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Tableau à une dimension
            // double[] tab  = null;
            // tab = new double[5];

            // double[] tab =  new double[5];

            int l = 5;  // La taille du tableau à la déclaration peut être une variable
            double[] tab = new double[l];

            // Valeur d'initialisation par défaut des éléments du tableau
            // entier -> 0
            // nombre à virgule flottante -> 0.0
            // char -> '\u0000'
            // bool -> false
            // type  référence -> null

            // Accèder à un élément du tableau
            Console.WriteLine(tab[0]);  //0.0
            tab[1] = 3.4;

            // Length=>Nombre d'élément du tableau
            Console.WriteLine(tab.Length);
            Console.WriteLine(tab.GetLength(0));

            // Déclaration et initialisation
            string[] tabStr = { "azerty", "hello", "world" };
            Console.WriteLine(tabStr[2]);

            // Parcourir complétement un tableau (avec un for)
            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine($"tab[{i}]={tab[i]}");
            }

            // Parcourir complétement un tableau (foreach)
            foreach (var e in tab)
            {
                Console.WriteLine(e);
                // e= 12.3; // e est accessible uniquement en lecture
            }

            //tab[100] = 12.5; // si l'on essaye d'accèder à un élément en dehors du tableau => exception
            #endregion

            #region  Tableau Multidimmension
            int[,] tab2D = new int[4, 3];   // Déclaration d'un tableau à 2 dimensions
            Console.WriteLine(tab2D[0, 1]);
            tab2D[1, 1] = 42;               // Accès à un élémént d'un tableau à 2 dimensions

            Console.WriteLine(tab2D.Length);        // Nombre d'élément du tableau    =>  12
            Console.WriteLine(tab2D.Rank);          // Nombre de dimension du tableau => 2
            Console.WriteLine(tab2D.GetLength(0));  // Nombre de ligne du tableau => 4
            Console.WriteLine(tab2D.GetLength(1));  // Nombre de colonne => 3

            // Parcourir complétement un tableau à 2 dimension => for
            for (int j = 0; l < tab.GetLength(0); l++)
            {
                for (int k = 0; k < tab.GetLength(1); k++)
                {
                    Console.Write($"{tab2D[j, k]} ");
                }
                Console.WriteLine();
            }

            // Parcourir complétement un tableau à 2 dimension => foreach
            foreach (var e in tab2D)
            {
                Console.WriteLine(e);
            }

            // Déclarer un tableau à 2 dimension en l'initialisant
            char[,] tab2Dchr = { { 'a', 'z' }, { 'e', 'r' }, { 't', 'y' } };
            foreach (var elm in tab2Dchr)
            {
                Console.WriteLine(elm);
            }

            // Tableau 3D
            // Déclaration d'un tableau à 3 dimensions
            int[,,] tab3D = new int[4, 2, 5];
            tab3D[2, 1, 1] = 12;                // Accès à un élément d'un tableau à 3 dimensions
            Console.WriteLine(tab3D.Rank);      // Nombre de dimension du tableau => 3
            for (int i = 0; i < tab3D.Rank; i++)
            {
                Console.WriteLine(tab3D.GetLength(i));
            }
            #endregion

            #region Tableau de tableau
            // Déclaration d'un tableau de tableau
            char[][] tabTc = new char[3][];
            tabTc[0] = new char[4];
            tabTc[1] = new char[2];
            tabTc[2] = new char[3];

            tabTc[1][0] = 'a';                      // accès à un élément
            Console.WriteLine(tabTc.Length);        // Nombre de Ligne => 3
            for (int i = 0; i < tabTc.Length; i++)  // Nombre d'élément de la première ligne
            {
                Console.WriteLine(tabTc[i].Length); // Nombre d'élément par ligne
            }

            //  Parcourir complétement un tableau de tableau => for
            for (int i = 0; i < tabTc.Length; i++)
            {
                for (int j = 0; j < tabTc[i].Length; j++)
                {
                    Console.Write($"{tabTc[i][j]}  ");
                }
                Console.WriteLine("");
            }

            //  Parcourir complétement un tableau de tableau => foreach
            foreach (var row in tabTc)
            {
                foreach (var elm in row)
                {
                    Console.WriteLine(elm);
                }
            }
            #endregion
            #region Indices Ranges
            int[] t2 = { 2, 5, 1, 8, 3, 0, 1, 5 };
            // Indices
            Console.WriteLine(t2[^1]); // dernier élément -> 5
            Index ind = ^2;
            Console.WriteLine(t2[ind]); // avant-dernier élément -> 1

            // Ranges
            foreach (var elm in t2[..2])
            {
                Console.WriteLine(elm);
            }

            foreach (var elm in t2[2..])
            {
                Console.WriteLine(elm);
            }

            Range r = 2..3;
            foreach (var elm in t2[r])
            {
                Console.WriteLine(elm);
            }
            #endregion
        }
    }
}
