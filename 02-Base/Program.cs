﻿using System;

namespace _01_Base
{
    enum Motorisation : short { ESSENCE = 95, DIESIEL = 50, GPL = 45, ELECTRIQUE = 1, HYDROGENE = 2 }

    [Flags]
    enum Jour
    {
        LUNDI = 1,
        MARDI = 2,
        MERCREDI = 4,
        JEUDI = 8,
        VENDREDI = 16,
        SAMEDI = 32,
        DIMANCHE = 64,
        WEEKEND = SAMEDI | DIMANCHE
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Variable et type simple
            // Déclaration d'une variable
            int i;
            //Console.WriteLine(i); // Erreur => on ne peut pas utiliser une variable locale non initialisée

            // Initialisation
            i = 42;
            Console.WriteLine(i);

            // Déclaration multiple de variable
            double hauteur = 3.5, largeur = 20.3;
            Console.WriteLine(hauteur + " " + largeur);

            // Littéral booléen
            bool test = false; // true
            Console.WriteLine(test);

            // Littéral caractère
            char chr = 'a';
            char utf8Chr = '\u0045';       // Caractère en UTF-8
            char utf8ChrHexa = '\x45';
            Console.WriteLine(chr + " " + utf8Chr + " " + utf8ChrHexa);

            // Littéral entier -> int par défaut
            long l = 12L;       // L->Littéral long
            uint ui = 1234U;    // U -> Litéral unsigned
            Console.WriteLine(l + " " + ui);

            // Littéral entier -> chagement de base
            int dec = 10;           // décimal (base 10) par défaut
            int hexa = 0xFF2;       // 0x => héxadécimal (base 16)
            int bin = 0b01010101;   // 0b => binaire 
            Console.WriteLine(dec + " " + hexa + " " + bin);

            // Littéral nombre à virgule flottante
            double d1 = 100.4;
            double d2 = 1.234e3;

            // littéral nombre à virgule floattante -> double
            float f = 12.3F;
            decimal deci = 12.3M;

            // Séparateur _
            int sep = 1_000_000;
            double sep2 = 1_234.50_01;  // pas de séparateur en début, en fin , avant et après la virgule
            Console.WriteLine(sep + " " + sep2);

            // Type implicite -> var
            var v1 = 10.2F;      // v1 -> float
            var v2 = "azerty";   // v2 -> string

            // avec @ on peut utiliser les mots réservés comme nom de variable (à éviter) 
            int @if = 12;
            Console.WriteLine(@if);
            #endregion
            #region Conversion
            // Transtypage implicite: ( pas de perte de donnée)
            // type inférieur => type supérieur
            int tii = 123;
            long til = tii;
            double tid = til;
            Console.WriteLine(tii + " " + til + " " + tid);

            // Transtypage explicite: cast = (nouveauType)
            double ted = 12.3;
            tii = (int)ted;
            decimal tedc = (decimal)ted;
            Console.WriteLine(ted + " " + tii + " " + tedc);

            // Dépassement de capacité
            // checked / unchecked
            // checked => checked permet d’activer explicitement le contrôle de dépassement de capacité 
            // pour les conversions et les opérations arithmétiques de type intégral

            // Il y a toujours un contrôle de dépassement de capacité  pour les constantes (sauf dans un bloc unchecked)

            unchecked // (par défaut)
            {
                // plus de vérification de dépassement de capacité
                int dep1 = 300; // 0000000000 0000000000 0000000001 000101100;
                sbyte sb1 = (sbyte)dep1;
                Console.WriteLine(dep1 + " " + sb1);

                const int max = int.MaxValue + 1;
            }

            //checked
            //{
            //    int dep1 = 300; // 0000000000 0000000000 0000000001 000101100;
            //    sbyte sb1 = (sbyte)dep1;  // avec checked une exception est générée, s'il y a un dépassement de capacité
            //    Console.WriteLine(dep1 + " " + sb1);
            //}

            // Fonction de convertion
            // La classe Convert contient des méthodes statiques permettant de convertir un entier vers un double, ...
            // On peut aussi convertir une chaine de caractères en un type numérique
            int fc1 = 42;
            double cnv1 = Convert.ToInt32(fc1); // Convertion d'un entier en double
            Console.WriteLine(cnv1);

            string str = "123";                  // Convertion d'une chaine de caractère  en entier
            int cnv2 = Convert.ToInt32(str);
            Console.WriteLine(cnv2);

            str = Convert.ToString(fc1);
            // Conversion d'un nombre sous forme de chaine dans une autre base
            Console.WriteLine(Convert.ToString(fc1, 2));    // affichage en binaire
            Console.WriteLine(Convert.ToString(fc1, 16));   // affichage en héxadécimal

            // Conversion d'une chaine de caractères en type simple
            // Parse
            Console.WriteLine(int.Parse("123"));
            //Console.WriteLine(int.Parse("123A"));   //  Erreur => génère une exception

            // TryParse

            int cnv3;
            Console.WriteLine(int.TryParse("123", out cnv3));   // Retourne true et la convertion est affecté à cnv3
            Console.WriteLine(cnv3);
            Console.WriteLine(int.TryParse("123A", out cnv3));  // Erreur => retourne false, 0 est affecté à cnv3
            Console.WriteLine(cnv3);
            #endregion

            #region Type nullable
            // Nullable : permet d'utiliser une valeur null avec un type valeur
            double? nd = null;

            Console.WriteLine(nd.HasValue);     // nd == null retourne false
            nd = 13.4;                          // Conversion implicite (double vers nullable)
            Console.WriteLine(nd.HasValue);     // La propriété HasValue retourne true si  nd contient une valeur (!= null)

            Console.WriteLine(nd.Value);    // Pour récupérer la valeur, on peut utiliser la propriété Value  
            Console.WriteLine((double)nd); // ou, on peut faire un cast
            #endregion

            #region Constante
            const double PI = 3.14;
            //PI=3.1419;      // Erreur: on ne peut pas modifier la valeur d'une constante
            Console.WriteLine(2 * PI);
            #endregion

            #region Enumération
            Motorisation mt = Motorisation.GPL;
            Console.WriteLine(mt);
            // enum -> string
            // La méthode toString convertit une constante enummérée en une chaine de caractère
            string strMt = mt.ToString();
            Console.WriteLine(strMt);

            // enum -> entier  (cast)
            short iMt = (short)mt;
            Console.WriteLine(iMt);

            // string -> enum
            // La méthode Parse permet de convertir une chaine de caractère en une constante enummérée
            Motorisation mt2 = (Motorisation)Enum.Parse(typeof(Motorisation), "HYDROGENE");
            Console.WriteLine(mt2);
            //mt2 = (Motorisation)Enum.Parse(typeof(Motorisation), "OXYGENE");  // si la chaine n'existe pas dans l'énumation => exception
            //Console.WriteLine(mt2);

            // entier -> enum
            iMt = 1;
            if (Enum.IsDefined(typeof(Motorisation), iMt)) // Permet de tester si la valeur entière existe dans l'enumération
            {
                mt2 = (Motorisation)iMt;
                Console.WriteLine(mt2);
            }

            // Énumération comme indicateurs binaires
            Jour jourSem = Jour.LUNDI | Jour.MERCREDI;
            Console.WriteLine((jourSem & Jour.LUNDI) != 0);  // teste la présence de LUNDI

            Console.WriteLine((jourSem & Jour.MARDI) != 0);  // teste la présence de MARDI

            jourSem |= Jour.SAMEDI;
            Console.WriteLine((jourSem & Jour.WEEKEND) != 0);
            Console.WriteLine(jourSem);
            #endregion

            #region Opérateur
            // Operateur arithméthique
            // int opArth = 3;
            // Console.WriteLine(opArth / 0); // entier division par 0 =>exception
            double opArthD = 2.0;
            double res = opArthD / 0.0;  // réel division par 0 => Infinity
            Console.WriteLine(double.IsInfinity(res)); // IsInfinity permet de tester res est égal à inifinity
            res = 0.0 / 0.0; //NaN
            Console.WriteLine(double.IsNaN(res));

            // Opérateur d'incrémentation
            // Pré-incrémentation
            int inc = 0;
            int r = ++inc;
            Console.WriteLine(inc + " " + r); // inc =1 r=1

            // Post-incrémentation
            inc = 0;
            r = inc++;
            Console.WriteLine(inc + " " + r); // r=0 inc =1

            // Affectation composée
            inc += 45; // équivaut à inc=inc+45
            Console.WriteLine(inc);

            // Opérateur de comparaison
            bool comp1 = inc > 100; // Une comparaison a pour résultat un booléen
            Console.WriteLine(comp1);

            // Opérateur logique
            // Opérateur Non
            Console.WriteLine(!(inc > 100)); //true

            // Opérateur court-circuit && et ||
            // && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
            bool tstA = inc > 100 && r == 0; //false
            Console.WriteLine(tstA);

            // idem avec || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées

            // Opération Binaire (bit à bit)
            byte b = 0b11010;
            Console.WriteLine(Convert.ToString(~b, 2));         // Opérateur ~ => complémént: 1-> 0 et 0-> 1
            Console.WriteLine(Convert.ToString(b & 0b110, 2));  //          et => 010
            Console.WriteLine(Convert.ToString(b ^ 0b110, 2));  // ou exclusif => 11100
            Console.WriteLine(Convert.ToString(b >> 2, 2));     // Décalage à droite de 2 => 110
            Console.WriteLine(Convert.ToString(b << 1, 2));     // Décalage à gauche de 1 => 110100

            // L’opérateur de fusion null ?? retourne la valeur de l’opérande de gauche si elle n’est pas null
            // sinon, il évalue l’opérande de droite et retourne son résultat
            string str2 = "azerty";
            Console.WriteLine(str2 ?? "default");
            str2 = null;
            Console.WriteLine(str2 ?? "default");

            // str2 est affecté uniquement si elle est égale à null (c# 8.0)
            str2 ??= "affectation";
            Console.WriteLine(str2);
            str2 ??= "Other";
            Console.WriteLine(str2);
            #endregion

            #region Promotion numérique
            // Le type le + petit est promu vers le +grand type des deux
            int prn = 2;
            double prn2 = 2.45;
            double resPrn = prn + prn2;
            Console.WriteLine(resPrn);
            resPrn = 5 / 2;
            Console.WriteLine(resPrn); // 2
            Console.WriteLine(5 / 2.0); // 2.5

            // sbyte, byte, short, ushort, char sont promus en int
            short s1 = 2;
            short s2 = 3;
            int s3 = s1 + s2;
            Console.WriteLine(s3);
            #endregion
        }
    }
}
